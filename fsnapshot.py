#!/opt/python-3.5.2/bin/python3.5

import setproctitle,sys,os, schedule, time
import subprocess, configparser, os, sys, time, re, smtplib, socket, requests
from email.mime.text import MIMEText
from daemon import Daemon

daemon = Daemon(
    user="root",
    group="root",
    stdin="/dev/null",
)

daemon.service()
config_file = configparser.RawConfigParser()
config_file.read(daemon.options.config)
files = os.path.split('snapshot_config')[1]
notif = eval(config_file.get(files,'notif'))
custom_script = eval(config_file.get(files,'script'))

def sendAlert(index,pesan):
    s = smtplib.SMTP('{smtp-address}')
    msg = MIMEText("""Snapshoot search \n\n %s \n\n\n Thanks,\n Boot""" % pesan)
    sender = notif['user_sendmail']
    recipients = notif['user_mailist']
    msg['Subject'] = "Snapshoot Index:" + index + ". Error !!"
    msg['From'] = sender
    msg['To'] = ", ".join(recipients)
    s.sendmail(sender, recipients, msg.as_string())

fd_log = None
fd_logdate = None
def log_file(logs):
    global daemon, fd_log, fd_logdate
    now = time.strftime("%Y%m%d", time.localtime())
    if not fd_log or now != fd_logdate:
       fd_logdate = now
       if fd_log: fd_log.close()
       fd_log = open(daemon.options.stdout + "." + fd_logdate, "a+")

    fd_log.write("%s\n" % (logs))
    fd_log.flush()

def job(t):
  print("I'm working...", t)
  for key, value in custom_script.items():
    try:
       gstatus = requests.get("http://{ip-search}:"+key.rsplit(':',1)[1],timeout=3) # Check search API status beafore backup global
       gcode = gstatus.status_code
    except Exception:
       gcode = 500

    if gcode !=200:
       if gcode == 500:
          sendAlert(key,"Timeout setelah 3 detik !!")
          log_file(key+": Timeout seletah 3 detik !!")
       else:
          sendAlert(key,"Error cluster status: "+str(gcode))
          log_file(key+": Error cluster status "+str(gcode))
    else:
       try:
          r = requests.get("http://{ip-search}:"+key.rsplit(':',1)[1]+"/_cat/health") # Check search API status beafore backup specified cluster name
          datas = r.text
          f = open("/tmp/listed-"+daemon.options.name_process+".conf","w")
          f.write(datas)
          f.close()

          open_list = open("/tmp/listed-"+daemon.options.name_process+".conf", "r")
          line = open_list.read()
          open_list.close()
          splitted = line.split()
          print("http://{ip-search}:"+key.rsplit(':',1)[1]+"/_cat/health")
          print(splitted[3])

          if splitted[3] != 'green':
             sendAlert(key,"Error cluster health, data not backup for : "+splitted[3])
             log_file(key+': Error cluster health, data not backup for :'+splitted[3]+' \n')
          else:
             try:
                print(key)
                command = value
                process = subprocess.Popen(command, shell=True, stdout=subprocess.PIPE)
                outfile = process.stdout.read()

                logs = open('/tmp/'+daemon.options.name_process+'-'+key,'w')
                logs.write(str(outfile.decode("utf-8")))
                logs.close()

                process.wait()
                time.sleep(120)

             except OSError:
                sendAlert(key,"Command yang dijalankan salah !!")
                log_file(key+': Command yang dijalankan salah !!\n')
       except Exception as e:
             sendAlert(key,"Command gagal di eksekusi system !!")
             log_file(key+': '+str(e)+'\n')
  return

if __name__ == "__main__":
       value = 22
       setproctitle.setproctitle(sys.argv[0])
       os.umask(value)
       config = daemon.options.config
       rootdir = daemon.options.rootdir
       sys.stdout.write("Using config: %s and root directory: %s\n" % (config,rootdir))
       sys.stdout.flush()

       schedule.every().day.at(daemon.options.time).do(job,'It is '+daemon.options.time)
       while True:
            schedule.run_pending()
            time.sleep(3) # wait 3 second
